<?php

namespace Zendos\ChuckNorrisJokes;

class JokeFactory
{

    protected $jokes = [
        'Chuck Norris threw a grenade and killed 50 people, then it exploded.',
        'Death once had a near-Chuck-Norris experience.',
        'This is a newly written joke about Chuck Noris'
    ];

    public function __construct(array $jokes = null)
    {

        if ($jokes) {
            $this->jokes = $jokes;
        }
    }

    public function getRandomJoke()
    {
        return $this->jokes[array_rand($this->jokes)];
    }
}
