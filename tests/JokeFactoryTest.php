<?php

namespace Zendos\ChuckNorrisJokes\Tests;

use Zendos\ChuckNorrisJokes\JokeFactory;

class JokeFactoryTest extends \PHPUnit\Framework\TestCase
{
    public function test_it_returns_a_random_joke()
    {
        $jokes = new JokeFactory([
            'This is a random joke',
        ]);
        $joke = $jokes->getRandomJoke();

        $this->assertSame('This is a random joke', $joke);
    }

    public function test_it_returns_a_predefined_joke()
    {

        $ChuckNorrisJokes =  [
            'Chuck Norris threw a grenade and killed 50 people, then it exploded.',
            'Death once had a near-Chuck-Norris experience.',
            'This is a new written joke about Chuck Noris'
        ];

        $jokes = new JokeFactory($ChuckNorrisJokes);
        $joke = $jokes->getRandomJoke();

        $this->assertContains($joke, $ChuckNorrisJokes);
    }
}
